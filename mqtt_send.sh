#!/bin/bash

while true; do 
  VORLAUF_TEMP=$(($RANDOM%10 + 10))
  RUECKLAUF_TEMP=$(($RANDOM%10))
  mosquitto_pub -h localhost -p 1883 -t sensornet/measurements -m "{ \"stage\": \"prod\", \"location\": \"cellar-heating\", \"device\": \"esp32-heating\", \"date\": \"2022-03-01\", \"measureno\": 1, \"vorlauf_temp\": $VORLAUF_TEMP, \"ruecklauf_temp\": $RUECKLAUF_TEMP }";
  sleep 0.2
done

