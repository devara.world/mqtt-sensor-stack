#!/bin/bash

docker exec -it influxdb influx remote create --org my-org --name remote-influxdb-slave --remote-url http://influxdb-slave:8086 --remote-api-token my-auth-token  --remote-org-id 4379dff276d8e534
docker exec -it influxdb influx replication create --org my-org --name replication-influxdb-slave --remote-id 0adb845ee5036000 --local-bucket-id c0cf590347195d98 --remote-bucket-id c0cf590347195d98

